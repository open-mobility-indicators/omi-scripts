These files describe initial data processing from fetching open data to creating the postgresql database, populating with data, preprocessing it, and creating n
ew data : a deadend flag attached to each OSM way, "blocks" made of non deadends ways forming polygons, attaching population census data to the blocks, etc.

The docs are in French, sorry...   
The 2 useful files [donnees-de-population](https://gitlab.com/open-mobility-indicators/omi-scripts/-/blob/master/db_creation_stepbystep/donnees-de-population.md) and [donnees-de-voirie](https://gitlab.com/open-mobility-indicators/omi-scripts/-/blob/master/db_creation_stepbystep/donnees-de-voirie.md).   
The other files are work in progress.   
    
This can be considered as a step-by-stop installation tutorial, the project aims at automating all data processing scripts so this should change a lot in the future.
