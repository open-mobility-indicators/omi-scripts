# Données INSEE  
Les données INSEE sont les données du recensement de la population et les données d'effectifs des établissements.

## Données de population:
https://www.insee.fr/fr/statistiques/2520034
on se limite à la population totale des rectangles dans le système compliqué d'anonymisation de l'Insee (11 hab mini par carreau de 200m sinon il sont regroupés dans des rectangles plus grands).
[Jean-Marc Viglino](https://www.data.gouv.fr/fr/datasets/csv-insee-carroyees-a-200-m-sur-la-population/)a géocodé l'information, c'est utile, merci à lui, mais j'essaie avec les rectangles importés dans postgis à partir des fichiers MIF/MID publiés, a priori ça sera plus simple pour calculer la population des mailles par simple intersection avec les rectangles.

En suivant la manip pour importer les fichiers MIF/MID des carreaux (et ) décrite par l'INSEE, puis en enregistrant la couche comme Shapfile, puis en important dans postgis

`ogr2ogr -f "PostgreSQL"  PG:"host=localhost dbname=osm user=postgres password=0pen@ccess!" popinsee200m.shp
`

On a une table popinsee200m avec une colonne pop qui est le nombre d'habitants dans le carré de 200mx200m (les autres champs ne sont disponibles que dans les rectangles pour préserver la vie privée).

## Données emplois
Pour les effectifs des entreprises, la base SIRENE (avec les établissements et leurs effectifs) a été [géocodée et republiée par Christian Quest](https://teamopendata.org/t/base-sirene-geocodee-quotidiennement/505), merci à lui! 

Récupération du fichier gz le plus récent sur http://data.cquest.org/geo_sirene/quotidien/
On ne garde que les colonnes avec l'ID, les effectifs et les coordonnées lon lat:
(j'ai changé le séparateur , en # pour que cut marche malgré les , dans les strings entre "...)
`cut -d '#' -f1,10,46,47,48,49,78,79,80,81,82,119,120 geo-sirene_2019148_E_Q2.csv > geosirene.csv`

Il y a des NN dans la colonne effectifs, on les remplace par des 0.
`sed -i s/#/,/g geosirene.csv
sed -i s/,\"NN\",/,0,/g geosirene.csv`

Puis import en base:
`COPY geosirene FROM '//home/patgendre/Documents/test-indic/insee/SIRENE/geosirene.csv' DELIMITERS ',' CSV HEADER;
ALTER TABLE geosirene ADD COLUMN geom geometry(POINT, 4326);
UPDATE geosirene SET geom = ST_SetSRID(ST_MakePoint(lon, lat) ,4326);
CREATE INDEX idx ON geosirene USING GIST ( geom );`

Même si c'est très approximatif on va utiliser le champ tefet pour compter les emplois.

