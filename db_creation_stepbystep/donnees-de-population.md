Extraire la donnée de population totale par carreau de 200 m des données Insee
Les [données Insee](https://www.insee.fr/fr/statistiques/2520034#consulter) ne sont pas pratiques à utiliser directement dans postgis.   
[Ce script](https://github.com/mecatran/insee-200m-extract) permet de convertir en un fichier csv ou geotiff facile à importer, il a été développé en 2015, mais depuis, les [données carroyées de l'INSEE ont été publiées directement en CSV](https://www.data.gouv.fr/fr/datasets/csv-insee-carroyees-a-200-m-sur-la-population/) par Jean-Marc Viglino.
La colonne ind_r correspond à la population totale. "Le fichier comporte les coordonnées du carreau (X,Y) à 100m en Lambert azimuthal equal-area (LAEA - EPSG:3035), les coordonnées sont donc (X*100, Y*100)." -> il reste à les convertir en latitude,longitude (EPSG 4326).   

On importe ce fichier CSV dans une table popinsee200m par COPY SQL, en gardant seulement la 3ème colonne ind_c (population totale du carreau de 200m).
```
cut -d',' -f1-3 carroyage-insee-metro-s2.csv > c-insee-metro_ind_c.csv
CREATE TABLE popinsee200m ( X float, Y float, ind_c float);
COPY popinsee200m (X, Y, ind_c) FROM '/home/patgendre/windows/Users/pmgen/Documents/infomobi/essais/test-indic/insee/200mcsv/c-insee-metro_ind_c.csv' DELIMITERS ',' CSV HEADER; 
```

