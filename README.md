# Open Mobility Indicators

Open Mobility Indicators is a set of free and collaborative software tools that process open data to allow the creation of applications that make visible the quality of accessibility of a neighborhood, a city, a region or any territory.

For the moment the project is documented in French but we will switch to English if needed!

The project has [a web site](https://open-mobility-indicators.gitlab.io/website/), and [a wiki](https://gitlab.com/open-mobility-indicators/omi-scripts/-/wikis/home); code includes [a kind of set-up/install documentation](https://gitlab.com/open-mobility-indicators/omi-scripts/-/tree/master/db_creation_stepbystep).

## License

[EUPL v1.2](https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12): akin to an AGPL, but more readable and translated and legally binding into all languages of the EU.
